# vue-firebase-auth

This is a vue-cli sample application with firebase authentication setup.

### Project setup
1. Create a firebase account
1. Enable Google SSO
1. Update your `.env` file with the contents of your [firebase config file](https://firebase.google.com/docs/web/setup#config-object)
1. `yarn install`

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
